﻿using Bespoke.Common.Net;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;

namespace Bespoke.Common.Osc
{
    // TODO Just a fast fix. Improvement needed.

    /// <summary>
    /// Represents a TCP/IP bi-directional client-side connection.
    /// </summary>
    public class OscBiDirectionalClient : OscClient
    {
        #region Attributes
        private List<string> registeredMethods;
        private ReaderWriterLockSlim registeredMethodsLock;
        private volatile bool handleMessages;
        #endregion

        #region Events

        /// <summary>
        /// Raised when an OscPacket is received.
        /// </summary>
        public event EventHandler<OscPacketReceivedEventArgs> PacketReceived;

        /// <summary>
        /// Raised when an OscBundle is received.
        /// </summary>
        public event EventHandler<OscBundleReceivedEventArgs> BundleReceived;

        /// <summary>
        /// Raised when an OscMessage is received.
        /// </summary>
        public event EventHandler<OscMessageReceivedEventArgs> MessageReceived;

        /// <summary>
        /// Raised when an error occurs during the reception of a packet.
        /// </summary>
        public event EventHandler<ExceptionEventArgs> ReceiveErrored;

        /// <summary>
        /// Raised when the connection is closed.
        /// </summary>
        public event EventHandler<TcpConnectionEventArgs> Disconnected;
        #endregion

        #region Properties
        /// <summary>
        /// Gets all registered Osc methods (address patterns).
        /// </summary>
        public string[] RegisteredMethods
        {
            get
            {
                registeredMethodsLock.EnterReadLock();
                string[] registeredMethodsArray = registeredMethods.ToArray();
                registeredMethodsLock.ExitReadLock();

                return registeredMethodsArray;
            }
        }

        /// <summary>
        /// Specifies if incoming Osc messages should be filtered against the registered methods.
        /// </summary>
        public bool FilterRegisteredMethods { get; set; }

        /// <summary>
        /// Gets or sets the handling of parsing exceptions.
        /// </summary>
        public bool ConsumeParsingExceptions { get; set; }
        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="OscBiDirectionalClient"/> class.
        /// </summary>
        /// <param name="serverEndPoint">The server-side endpoint of the connection.</param>
        public OscBiDirectionalClient(IPEndPoint serverEndPoint)
            : this(serverEndPoint.Address, serverEndPoint.Port)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="OscBiDirectionalClient"/> class.
        /// </summary>
        /// <param name="serverIPAddress">The server-side IP address of the connection.</param>
        /// <param name="serverPort">The server-side port of the connection.</param>
        public OscBiDirectionalClient(IPAddress serverIPAddress, int serverPort)
            : base(serverIPAddress, serverPort)
        {
            registeredMethods = new List<string>();
            registeredMethodsLock = new ReaderWriterLockSlim();
        }

        /// <summary>
        /// Connect to the previously specified server-side endpoint.
        /// </summary>
        public new void Connect()
        {
            Connect(RemoteIPAddress, RemotePort);
        }

        /// <summary>
        /// Connect to the previously specified server-side endpoint.
        /// </summary>
        /// <param name="serverEndPoint">The server-side endpoint to connect to.</param>
        public new void Connect(IPEndPoint serverEndPoint)
        {
            Connect(serverEndPoint.Address, serverEndPoint.Port);
        }

        /// <summary>
        /// Connect to a server.
        /// </summary>
        /// <param name="serverIPAddress">The server-side IP address to connect to.</param>
        /// <param name="serverPort">The server-side port to connect to.</param>
        public new void Connect(IPAddress serverIPAddress, int serverPort)
        {
            base.Connect(serverIPAddress, serverPort);

            handleMessages = true;

            Connection.DataReceived += OnDataReceived;
            Connection.Disconnected += OnDisconnected;
            Connection.ReceiveDataAsync();
        }

        /// <summary>
        /// Close the connection.
        /// </summary>
        public new void Close()
        {
            base.Close();
            handleMessages = false;
        }

        /// <summary>
        /// Register an Osc method.
        /// </summary>
        /// <param name="method">The Osc address pattern to register.</param>
        public void RegisterMethod(string method)
        {
            registeredMethodsLock.EnterWriteLock();
            if (registeredMethods.Contains(method) == false)
            {
                registeredMethods.Add(method);
            }
            registeredMethodsLock.ExitWriteLock();
        }

        /// <summary>
        /// Unregister an Osc method.
        /// </summary>
        /// <param name="method">The Osc address pattern to unregister.</param>
        public void UnRegisterMethod(string method)
        {
            registeredMethodsLock.EnterWriteLock();
            registeredMethods.Remove(method);
            registeredMethodsLock.ExitWriteLock();
        }

        #region Private Methods

        /// <summary>
        /// Process disconnected events.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        private void OnDisconnected(object sender, TcpConnectionEventArgs e)
        {
            Disconnected(e.Connection, e);
        }

        /// <summary>
        /// Process data received events.
        /// </summary>
        /// <param name="sender">The sender of the event.</param>
        /// <param name="e">An EventArgs object that contains the event data.</param>
        private void OnDataReceived(object sender, TcpDataReceivedEventArgs e)
        {
            DataReceived(e.Connection, (IPEndPoint)e.Connection.Client.RemoteEndPoint, e.Data);
        }

        /// <summary>
        /// Process the data received event.
        /// </summary>
		/// <param name="connection">The <see cref="TcpConnection" /> object associated with this data.</param>
        /// <param name="sourceEndPoint">The source endpoint.</param>
        /// <param name="data">The received data.</param>
        private void DataReceived(TcpConnection connection, IPEndPoint sourceEndPoint, byte[] data)
        {
            if (handleMessages)
            {
                try
                {
                    OscPacket packet = OscPacket.FromByteArray(sourceEndPoint, data);
                    packet.Client = new OscClient(connection);

                    OnPacketReceived(packet);

                    if (packet.IsBundle)
                    {
                        OnBundleReceived(packet as OscBundle);
                    }
                    else
                    {
                        if (FilterRegisteredMethods)
                        {
                            registeredMethodsLock.EnterReadLock();
                            if (registeredMethods.Contains(packet.Address))
                            {
                                OnMessageReceived(packet as OscMessage);
                            }
                            registeredMethodsLock.ExitReadLock();
                        }
                        else
                        {
                            OnMessageReceived(packet as OscMessage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    if (ConsumeParsingExceptions == false)
                    {
                        OnReceiveErrored(ex);
                    }
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="PacketReceived"/> event.
        /// </summary>
        /// <param name="packet">The packet to include in the event arguments.</param>
        private void OnPacketReceived(OscPacket packet)
        {
            if (PacketReceived != null)
            {
                PacketReceived(this, new OscPacketReceivedEventArgs(packet));
            }
        }

        /// <summary>
        /// Raises the <see cref="BundleReceived"/> event.
        /// </summary>
        /// <param name="bundle">The packet to include in the event arguments.</param>
        private void OnBundleReceived(OscBundle bundle)
        {
            if (BundleReceived != null)
            {
                BundleReceived(this, new OscBundleReceivedEventArgs(bundle));
            }

            foreach (object value in bundle.Data)
            {
                if (value is OscBundle)
                {
                    // Raise events for nested bundles
                    OnBundleReceived((OscBundle)value);
                }
                else if (value is OscMessage)
                {
                    // Raised events for contained messages
                    OscMessage message = (OscMessage)value;
                    if (FilterRegisteredMethods)
                    {
                        registeredMethodsLock.EnterReadLock();
                        if (registeredMethods.Contains(message.Address))
                        {
                            OnMessageReceived(message);
                        }
                        registeredMethodsLock.ExitReadLock();
                    }
                    else
                    {
                        OnMessageReceived(message);
                    }
                }
            }
        }

        /// <summary>
        /// Raises the <see cref="MessageReceived"/> event.
        /// </summary>
        /// <param name="message">The message to include in the event arguments.</param>
        private void OnMessageReceived(OscMessage message)
        {
            if (MessageReceived != null)
            {
                MessageReceived(this, new OscMessageReceivedEventArgs(message));
            }
        }

        /// <summary>
        /// Raises the <see cref="ReceiveErrored"/> event.
        /// </summary>
        /// <param name="ex">The associated exception.</param>
        private void OnReceiveErrored(Exception ex)
        {
            if (ReceiveErrored != null)
            {
                ReceiveErrored(this, new ExceptionEventArgs(ex));
            }
        }

        #endregion
    }
}
